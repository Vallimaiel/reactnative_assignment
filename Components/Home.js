import React, { Component } from 'react';
import { Text, View,Animated,Easing } from 'react-native'
import { Card } from 'react-native-elements'
import { connect } from 'react-redux'
import { baseUrl } from '../Shared/baseUrl'
import { Loading } from './LoadingComponent';
//featured==true
const mapStateToProps = state => {
    console.log("hhh", state.promotions)
    return {
        dishes: state.dishes,
        leaders: state.leaders,
        promos: state.promotions

    }
}
function RenderItem(props) {
    if (props.isloading) {
        return (
            <Loading />)
    }
    else if (props.errMess) {
        return (
            <View>
                <Text>{props.errMess}</Text>
            </View>)
    }
    else {
        if (props.item != null) {
            return (
                <Card
                    featuredTitle={props.item.name}
                    featuredSubtitle={props.item.designation}
                    image={{ uri: baseUrl + props.item.image }}>
                    <Text>{props.item.description}</Text>
                </Card>)
        }
        else {
            return (<View></View>)
        }
    }
}

class HomeComponent extends Component {
    constructor(props) {
        super(props)
        this.animatedValue=new Animated.Value(0)
    }
    static navigationOptions = {
        title: 'Home'
    }
    componentDidMount()
    {
        this.animate()
    }
    animate()
    {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue:8,
                duration:8000,
                easing:Easing.linear
            }
        ).start(()=>this.animate())
    }
    render() {
        const xpos1=this.animatedValue.interpolate({
            inputRange:[0,1,3,5,8],
            outputRange:[1200,600,0,-600,-1200]
        })
        const xpos2=this.animatedValue.interpolate({
            inputRange:[0,2,4,6,8],
            outputRange:[1200,600,0,-600,-1200]
        })
        const xpos3=this.animatedValue.interpolate({
            inputRange:[0,3,5,7,8],
            outputRange:[1200,600,0,-600,-1200]
        })
        return (
            <View style={{flex:1,flexDirection:'row',justifyContent:'center'}}>
            <Animated.View style={{width:'100%',transform:[{translateX:xpos1}]}}>
                <RenderItem item={this.props.dishes.dishes.filter((dish) => dish.featured)[0]} isloading={this.props.dishes.isLoading} errMess={this.props.dishes.errMess} />
            </Animated.View>
            <Animated.View style={{width:'100%',transform:[{translateX:xpos2}]}}>
                <RenderItem item={this.props.promos.promotions.filter((promo) => promo.featured)[0]} isloading={this.props.promos.isLoading} errMess={this.props.promos.errMess} />
            </Animated.View>
            <Animated.View style={{width:'100%',transform:[{translateX:xpos3}]}}>
                <RenderItem item={this.props.leaders.leaders.filter((leader) => leader.featured)[0]} isloading={this.props.leaders.isLoading} errMess={this.props.leaders.errMess} />
            </Animated.View>
            </View>
        )
    }
}
export default connect(mapStateToProps)(HomeComponent)