import React,{Component} from 'react'
import {View,Text,ScrollView,Switch,Picker,StyleSheet,Modal} from 'react-native' 
import DatePicker from 'react-native-datepicker'
import { Button } from 'react-native-elements';
export default class ReservationComponent extends Component{
    constructor(props){
        super(props)
        this.state={
            guests:1,
            date:'',
            smoking:false,
            showModal:false
        }
    }
    static navigationOptions = {
        title:'Reserve Table'
    }
    handleReservation=()=>{
        console.log(JSON.stringify(this.state))
        this.toggleModal()
    }
    toggleModal=()=>{
        this.setState({
            showModal:!this.state.showModal
        })
    }
    resetForm=()=>{
        this.setState({
            guests:1,
            date:'',
            smoking:false
        })
    }
    render(){
        return(
            <ScrollView>
                <View style={styles.formRow}>
                <Text style={styles.formLabel}>Number of Guests</Text>
                <Picker
                    style={styles.formItem}
                    selectedValue={this.state.guests}
                    onValueChange={(itemValue,itemIndex)=>this.setState({guests:itemValue})}>
                    <Picker.Item label = '1' value='1'/>
                    <Picker.Item label = '2' value='2'/>
                    <Picker.Item label = '3' value='3'/>
                    <Picker.Item label = '4' value='4'/>
                    <Picker.Item label = '5' value='5'/>
                    <Picker.Item label = '6' value='6'/>
                    </Picker>
                </View>
                <View style={styles.formRow}>
                <Text style={styles.formLabel}>Smoking/Non-Smoking?</Text>
                <Switch
                    style={styles.formItem}
                    value={this.state.smoking}
                    trackColor='#512DA8'
                    onValueChange={(value)=>this.setState({smoking:value})}>
                </Switch>
                </View>
                <View style={styles.formRow}>
                <Text style={styles.formLabel}>Date and Time</Text>
                <DatePicker
                    style={{flex:2,marginRight:20}}
                    date={this.state.date}
                    format=''
                    mode='datetime'
                    placeholder='select date and time'
                    minDate='2017-01-01'
                    confirmBtnText='Confirm'
                    cancelBtnText='Cancel'
                    customStyles={{
                        dateIcon:{
                            position:'absolute',
                            left:0,
                            top:4,
                            marginLeft:0
                        },
                        dateInput:{
                            marginLeft:36
                        }
                    }}
                    onDateChange={(date)=>{this.setState({date:date})}}
                    />
                </View>
                <View style={styles.formRow}>
                <Button
                    title='Reserve'
                    onPress={()=>this.handleReservation()}
                    color='#512DA8'
                    accessibilityLabel="Learn more about this purple button"
                    />
                </View>
                <Modal animationType={"slide-up"} visible={this.state.showModal} transparent={false} onDismiss={()=>this.toggleModal()} onRequestClose={()=>this.toggleModal()}>
                    <View style={styles.modal}>
                        <Text style={styles.modalTitle}>Your Reservation</Text>
                        <Text style={styles.modalText}>Number of Guests:{this.state.guests}</Text>
                        <Text style={styles.modalText}>Smoking?:{this.state.smoking?"Yes":"No"}</Text>
                        <Text style={styles.modalText}>Date and Time :{this.state.date}</Text>
                        <Button onPress={()=>{this.toggleModal(),this.resetForm()}} title='CLOSE' color='#512DAB'/>
                    </View>
                </Modal>
            </ScrollView>

        )
    }
}
const styles=StyleSheet.create({
    formRow:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row',
        margin:20
    },
    formLabel:{
        flex:2,
        fontSize:18
    },
    formItem:{
        flex:1
    },
    modal:{
        justifyContent:'center',
        margin:20
    },
    modalTitle:{
        color:'white',
        textAlign:'center',
        marginBottom:20,
        backgroundColor:'#512DAB',
        fontWeight:'bold',
        fontSize:24
    },
    modalText:{
        margin:10,
        fontSize:18
    }
})
