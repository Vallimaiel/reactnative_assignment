import React, { Component } from 'react'
import { Card, ListItem } from 'react-native-elements'
import { ScrollView, Text, FlatList } from 'react-native'
import { connect } from 'react-redux'
import baseUrl from '../Shared/baseUrl'
import * as Animatable from 'react-native-animatable'
import { Loading } from './LoadingComponent';
const mapStateToProps = state => {
    return {
        leaders: state.leaders
    }
}
function History() {
    return (
        <Card
            title={'Our History'} >
            <Text>Started in 2010, Ristorante con Fusion quickly established itself as a culinary icon par excellence in Hong Kong. With its unique brand of world fusion cuisine that can be found nowhere else, it enjoys patronage from the A-list clientele in Hong Kong.  Featuring four of the best three-star Michelin chefs in the world, you never know what will arrive on your plate the next time you visit us.
{"\n"}</Text>
            <Text>The restaurant traces its humble beginnings to The Frying Pan, a successful chain started by our CEO, Mr. Peter Pan, that featured for the first time the world's best cuisines in a pan.
</Text>
        </Card>
    )
}

class AboutComponent extends Component {

    static navigationOptions = {
        title: 'About Us'
    }
    render() {
        renderLeaders = ({ item, index }) => {
            return (
                <ListItem
                    key={index}
                    title={item.name}
                    titleStyle={{ fontWeight: 'bold', color: 'black' }}
                    subtitle={item.description}
                    hideChevron={true}
                    leftAvatar={{ source: require('./images/alberto.png') }}
                />
            )
        }
        if (this.props.leaders.isLoading) {
            return (
                <ScrollView>
                    <History />
                    <Card
                        title={'Corporate Leadership'}>/>
                        <Loading/>
                   </Card>
                    <Text>{"\n"}</Text>
                </ScrollView>

            )
        }
        else if(this.props.leaders.errMess)
        {
            return(
                <ScrollView>
                    <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                    <History />
                    <Card
                        title={'Corporate Leadership'}>/>
                        <Text>{this.props.leaders.errMess}</Text>
                   </Card>
                    <Text>{"\n"}</Text>
                    </Animatable.View>
                </ScrollView>
            )
        }
        else
        {
        return (
            <ScrollView>
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                <History />
                <Card
                    title={'Corporate Leadership'}>
                    <FlatList
                        data={this.props.leaders.leaders}
                        renderItem={renderLeaders}
                        keyExtractor={item => item.id.toString()}
                    />
                </Card>
                <Text>{"\n"}</Text>
                </Animatable.View>
            </ScrollView>
        )
        }
    }
}
export default connect(mapStateToProps)(AboutComponent)