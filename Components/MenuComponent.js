import React from 'react';
import { View, FlatList ,Text} from 'react-native';
import { Tile } from 'react-native-elements';
import { connect } from 'react-redux'
import {baseUrl} from '../Shared/baseUrl'
import { Loading } from './LoadingComponent';
import * as Animatable from 'react-native-animatable'
const mapStateToProps=state=>{
    return{
        dishes:state.dishes
    }
}

class MenuComponent extends React.Component {
    constructor(props){
        super(props)
        
    }
    static navigationOptions={
        title:'Menu'
    }
    
    
    render(){
        const {navigate}=this.props.navigation
        renderMenuItem = ({item, index}) => {
            return (
                <Animatable.View animation='fadeInRightBig' duration={2000}>
                    <Tile
                        key={index}
                        title={item.name}
                        titleStyle={{color:'black',fontWeight:'bold'}}
                        caption={item.description}
                        featured
                        onPress={()=>navigate('DishDetail',{dishId:item.id})}
                        imageSrc={{uri:baseUrl+item.image}}
                      />
                      </Animatable.View>
            );
        };
    if(this.props.dishes.isLoading)
    {
        return (
            <Loading/>
        ); 
    }
    else if(this.props.dishes.errMess) 
    {
        return(
            <View>
                <Text>{this.props.dishes.errMess}</Text>
            </View>
        )
    }
    else
    {
    return (
            <FlatList 
                data={this.props.dishes.dishes}
                renderItem={renderMenuItem}
                keyExtractor={item => item.id.toString()}
                />
    );
    }
    }
}

export default connect(mapStateToProps)(MenuComponent)



